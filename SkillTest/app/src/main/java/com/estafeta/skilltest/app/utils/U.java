package com.estafeta.skilltest.app.utils;

import android.text.TextUtils;

import org.w3c.dom.Text;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * U утилиты разного рода
 */

public class U {
    public static Date dateStringValid (String pString, SimpleDateFormat pSDF) throws ParseException {
        return !TextUtils.isEmpty(pString) && !pString.equals("null") ? pSDF.parse(pString) : new Date(0);
    }
}
