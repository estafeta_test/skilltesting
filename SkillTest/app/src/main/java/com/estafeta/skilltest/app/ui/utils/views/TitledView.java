package com.estafeta.skilltest.app.ui.utils.views;

import android.content.Context;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.estafeta.skilltest.app.R;

/**
 * Created by Doosikus on 04.08.2016.
 * TitledView
 */

public class TitledView extends LinearLayout {

    private final TextView mTextView;

    public TitledView(Context context) {
        this(context, null);
    }

    public TitledView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public TitledView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        LayoutInflater inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View v = inflater.inflate(R.layout.view_titledview, this, true);
        ((TextView)v.findViewById(R.id.tv_title_titledview)).setText(TextUtils.isEmpty((String)getTag()) ? "" : (String)getTag());
        mTextView = (TextView)v.findViewById(R.id.tv_deskription_titledview);
        mTextView.setText(" "); // не пустая строка
    }

    public void setTextView(String pTextView) {
        mTextView.setText(pTextView);
    }




}
