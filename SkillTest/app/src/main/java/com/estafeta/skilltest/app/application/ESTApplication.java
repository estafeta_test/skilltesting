package com.estafeta.skilltest.app.application;

import android.app.Application;
import android.content.Context;

import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;

import io.realm.Realm;
import io.realm.RealmConfiguration;

/**
 * @author doosik
 *         ESTApplication
 */
public class ESTApplication extends Application {


    private static Context sContext;
    private RequestQueue mRequestQueue;

    @Override
    public void onCreate() {
        super.onCreate();
        sContext = getApplicationContext();
        mRequestQueue = Volley.newRequestQueue(this);
        mRequestQueue.start();

        RealmConfiguration realmConfiguration = new RealmConfiguration.Builder(this)
                .schemaVersion(1)
                .deleteRealmIfMigrationNeeded()
                .build();
        Realm.setDefaultConfiguration(realmConfiguration);

    }

    public static Context getContext() {
        return sContext;
    }

    public RequestQueue getRequestQueue() {
        return mRequestQueue;
    }
}
