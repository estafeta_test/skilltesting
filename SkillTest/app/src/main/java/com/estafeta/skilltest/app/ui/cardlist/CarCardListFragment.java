package com.estafeta.skilltest.app.ui.cardlist;

import android.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.estafeta.skilltest.app.R;
import com.estafeta.skilltest.app.entity.CarCard;
import com.estafeta.skilltest.app.entity.helpers.CarCardHelper;
import com.estafeta.skilltest.app.request.CarListRequest;
import com.estafeta.skilltest.app.ui.cardinfo.CardInfoFragment;
import com.estafeta.skilltest.app.ui.utils.activitys.MainActivity;
import com.estafeta.skilltest.app.ui.utils.listners.OnAdapterClickListener;
import com.estafeta.skilltest.app.utils.log;

import io.realm.RealmResults;

/**
 * Created by Doosikus on 04.08.2016.
 * CarCardListFragment
 */

public class CarCardListFragment extends Fragment implements OnAdapterClickListener<CarCard> {


    private static final String TAG = CarCardListFragment.class.getSimpleName();


    private ListView mList;
    private CarCardHelper mCardListHelper;
    private CardListAdapter mCardListAdapter;
    private RealmResults<CarCard> mRealmResult;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        MainActivity activity = (MainActivity) getActivity();
        activity.setTitle(R.string.car_card_list_fragment_title);
        CarListRequest.Send();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        ViewGroup view = (ViewGroup) inflater.inflate(R.layout.fragment_cardlist, container, false);
        mList = (ListView) view.findViewById(R.id.list);
        return view;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mCardListHelper = new CarCardHelper(((MainActivity)getActivity()).getRealm());
        mRealmResult = mCardListHelper.findAll();
        mCardListAdapter = new CardListAdapter(getActivity(), mRealmResult, this);
        log.dt(TAG, String.format("List size: %d", mRealmResult.size()));


        mList.setAdapter(mCardListAdapter);
        setHasOptionsMenu(true);

    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.m_main, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.mi_sync:
                CarListRequest.Send();
                break;
            case R.id.mi_search:
                //TODO ActionMode open
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClickAdapter(int pViewId, CarCard entity, View view) {
       if (entity != null && entity.isValid()) {
           log.dt(TAG, String.format("Id: %d, Vin: %s", entity.getId(), entity.getVin()));
           Bundle args = new Bundle();
           args.putLong(CarCardHelper.I_CARD_ID, entity.getId());
           ((MainActivity)getActivity()).showPresentations(CardInfoFragment.class.getName(), true, args);
       }
    }
}
