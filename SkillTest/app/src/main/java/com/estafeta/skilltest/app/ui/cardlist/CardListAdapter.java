package com.estafeta.skilltest.app.ui.cardlist;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.estafeta.skilltest.app.R;
import com.estafeta.skilltest.app.entity.CarCard;
import com.estafeta.skilltest.app.ui.utils.adapters.BaseHolderAdapter;
import com.estafeta.skilltest.app.ui.utils.listners.OnAdapterClickListener;

import java.text.SimpleDateFormat;

import io.realm.RealmResults;

/**
 * Created by Doosikus on 04.08.2016.
 * CardListAdapter
 */

public class CardListAdapter extends BaseHolderAdapter<CarCard, CardListAdapter.CardHolder> {

private SimpleDateFormat mSDF;

    public CardListAdapter(Context context, RealmResults<CarCard> realmResults, OnAdapterClickListener<CarCard> pAdapterClickListener) {
        super(context, realmResults, pAdapterClickListener);
        mSDF = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
    }

    @Override
    public CardHolder onCreateViewHolder(LayoutInflater inflater, ViewGroup parent, int pViewType) {
        return new CardHolder(inflater.inflate(R.layout.item_layout, parent, false), pViewType);
    }

    @Override
    public void onBindViewHolder(CardHolder holder, CarCard entity) {
        holder.code.setText(entity.getModelCode());
        holder.vin.setText(entity.getVin());
        holder.mark.setText(String.format("%s / %s", entity.getBrand(), entity.getModel()));
        holder.date.setText(mSDF.format(entity.getActualEndDate()));
        holder.driver.setText(entity.getDriver());
        setOnClickListener(holder.rootView, holder.getPosition());
    }

    class CardHolder extends BaseHolderAdapter.ViewHolder {

        public final TextView vin, date, code, driver, mark;
        public final View rootView;

        public CardHolder(View pView, int pViewType) {
            super(pView, pViewType);
            vin = (TextView) findViewById(R.id.tv_vin_item_layout);
            date = (TextView) findViewById(R.id.tv_date_item_layout);
            code = (TextView) findViewById(R.id.tv_code_item_layout);
            driver = (TextView) findViewById(R.id.tv_driver_item_layout);
            mark = (TextView) findViewById(R.id.tv_mark_item_layout);
            rootView = pView;
        }


    }
}
