package com.estafeta.skilltest.app.ui.utils.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.estafeta.skilltest.app.ui.utils.listners.OnAdapterClickListener;
import com.estafeta.skilltest.app.utils.log;

import io.realm.RealmBaseAdapter;
import io.realm.RealmObject;
import io.realm.RealmResults;

/**
 * Created by Doosikus on 04.08.2016.
 */

public abstract class BaseHolderAdapter<T extends RealmObject, H extends BaseHolderAdapter.ViewHolder> extends RealmBaseAdapter<T> implements View.OnClickListener {
    protected final OnAdapterClickListener<T> mAdapterClickListener;

    public BaseHolderAdapter(Context context, RealmResults<T> realmResults, OnAdapterClickListener<T> pAdapterClickListener) {
        super(context, realmResults, true);
        mAdapterClickListener = pAdapterClickListener;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        H tag = convertView != null ? (H) convertView.getTag() : null;
        T entity = realmResults.get(position);
        if (tag == null || tag.getViewType() != getViewType(entity)) {
            tag = onCreateViewHolder(inflater, parent, getViewType(entity));
        }

        tag.setPosition(position);

        onBindViewHolder(tag, entity);

        return tag.rootView;
    }

    protected int getViewType(T pEntity) {
        return 0;
    }

    protected void setOnClickListener(View view, int position) {
        view.setOnClickListener(this);
    }

    public abstract H onCreateViewHolder(LayoutInflater inflater, ViewGroup parent, int pViewType);

    public abstract void onBindViewHolder(H holder, T entity);

    public int getPosition(T pEntity) {
        for (int i = 0; i < realmResults.size(); i++){
            if (realmResults.get(i).equals(pEntity)) {
                return i;
            }
        };
        return -1;
    }

    @Override
    public void onClick(View v) {
        if (mAdapterClickListener != null && v != null) {
            final int index = ((ViewHolder)v.getTag()).getPosition();
            if (index >= 0 && index < realmResults.size()) {
                mAdapterClickListener.onClickAdapter(v.getId(), realmResults.get(index), v);
            } else {
                log.w("Index out of bounds: %d in %s", v.getTag(), realmResults.size());
            }
        }
    }

    public static class ViewHolder {
        public final View rootView;
        private int mPosition;
        private int mViewType;

        public ViewHolder(View pView) {
            this(pView, 0);
        }

        public ViewHolder(View pView, int pViewType) {
            rootView = pView;
            rootView.setTag(this);
            mViewType = pViewType;
        }

        public View findViewById(int id) {
            return rootView.findViewById(id);
        }

        public int getPosition() {
            return mPosition;
        }

        public void setPosition(int position) {
            mPosition = position;
        }

        public int getViewType() {
            return mViewType;
        }

        public void setViewType(int pViewType) {
            mViewType = pViewType;
        }
    }
}