package com.estafeta.skilltest.app.ui.utils.listners;

import android.view.View;

import io.realm.RealmObject;

/**
 * Created by Doosikus on 04.08.2016.
 * OnAdapterClickListener
 */

public interface OnAdapterClickListener<T extends RealmObject> {
    void onClickAdapter(int pViewId, T entity, View view);
}
