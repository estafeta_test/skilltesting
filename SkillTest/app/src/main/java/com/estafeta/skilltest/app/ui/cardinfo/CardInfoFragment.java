package com.estafeta.skilltest.app.ui.cardinfo;

import android.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import com.estafeta.skilltest.app.R;
import com.estafeta.skilltest.app.entity.CarCard;
import com.estafeta.skilltest.app.entity.helpers.CarCardHelper;
import com.estafeta.skilltest.app.ui.cardlist.CarCardListFragment;
import com.estafeta.skilltest.app.ui.utils.activitys.MainActivity;
import com.estafeta.skilltest.app.ui.utils.views.TitledView;
import com.estafeta.skilltest.app.utils.log;

import java.text.SimpleDateFormat;

/**
 * CardInfoFragment
 */

public class CardInfoFragment extends Fragment {

    private static final String TAG = CarCardListFragment.class.getSimpleName();
    private long mId;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        MainActivity activity = (MainActivity) getActivity();
        activity.setTitle(R.string.car_card_info_title);

        if (savedInstanceState != null && savedInstanceState.containsKey(CarCardHelper.I_CARD_ID)) {
            mId = savedInstanceState.getLong(CarCardHelper.I_CARD_ID);
        } else {
            Bundle args = getArguments();
            if (args != null && args.containsKey(CarCardHelper.I_CARD_ID)) {
                mId = args.getLong(CarCardHelper.I_CARD_ID);
            }
        }

        log.dt(TAG, String.format("Id: %d", mId));
        setHasOptionsMenu(true);

    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle
            savedInstanceState) {

        CarCard carCard = new CarCardHelper(((MainActivity)getActivity()).getRealm()).getEntityById(mId);

        if (carCard == null || !carCard.isValid()) {
            //TODO обработать нормально исключение
        }

        ViewGroup view = (ViewGroup) inflater.inflate(R.layout.fragment_cardinfo, container, false);
        setTitleEntity(view, R.id.tv_task_cardinfo_fragment, "After Unloading"); //FIXME wtf ... откуда береться .. уточнить

        SimpleDateFormat SDF = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");

        setTitleEntity(view, R.id.tv_starttime_cardinfo_fragment, SDF.format(carCard.getPlannedStartDate()));
        setTitleEntity(view, R.id.tv_survey_cardinfo_fragment, SDF.format(carCard.getActualEndDate()));
        setTitleEntity(view, R.id.tv_vin_cardinfo_fragment, carCard.getVin());
        setTitleEntity(view, R.id.tv_brand_cardinfo_fragment, carCard.getBrand());
        setTitleEntity(view, R.id.tv_model_cardinfo_fragment, carCard.getModel());
        setTitleEntity(view, R.id.tv_modelcode_cardinfo_fragment, carCard.getModelCode());
        setTitleEntity(view, R.id.tv_inspection_cardinfo_fragment, carCard.getSurveyPoint());

        return view;
    }

    private void setTitleEntity(ViewGroup pView, int pId, String pText) {
        ((TitledView)pView.findViewById(pId)).setTextView(pText);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        menu.clear();
        inflater.inflate(R.menu.m_main_cardinfo, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.mi_send:
                //TODO какойто сенд
                break;
        }
        return super.onOptionsItemSelected(item);
    }

}
