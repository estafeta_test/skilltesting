package com.estafeta.skilltest.app.entity.helpers;

import com.estafeta.skilltest.app.entity.CarCard;
import com.estafeta.skilltest.app.utils.Settings;
import com.estafeta.skilltest.app.utils.U;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Locale;

import io.realm.Realm;
import io.realm.exceptions.RealmException;

/**
 * Created by Doosikus on 03.08.2016.
 * CarCardHelper
 */
public class CarCardHelper extends BaseHelper<CarCard> {
    public CarCardHelper(Realm pRealm) {
        super(pRealm, CarCard.class);
    }
    public static final String I_CARD_ID = "I_CARD_ID";

    @Override
    public CarCard createOrUpdateFromJson(JSONObject pJson) throws RealmException, JSONException, ParseException {
        SimpleDateFormat sdf = new SimpleDateFormat(Settings.DATE_PATTERN, Locale.getDefault());
        long id = pJson.getLong("Id");
        CarCard card = getEntityById(id);
        if (card == null) {
            card = create();
            card.setId(id);
        }
        card.setNumber(pJson.getString("Number"));

        String plannedStartDate = pJson.getString("PlannedStartDate");
        card.setPlannedStartDate(U.dateStringValid(plannedStartDate, sdf));


        String plannedEndDate = pJson.getString("PlannedEndDate");
        card.setPlannedStartDate(U.dateStringValid(plannedEndDate, sdf));

        String actualStartDate = pJson.getString("ActualStartDate");
        card.setPlannedStartDate(U.dateStringValid(actualStartDate, sdf));


        String actualEndDate = pJson.getString("ActualEndDate");
        card.setPlannedStartDate(U.dateStringValid(actualEndDate, sdf));

        card.setVin(pJson.getString("Vin"));

        card.setModel(pJson.getString("Model"));
        card.setModelCode(pJson.getString("Vin"));
        card.setBrand(pJson.getString("Brand"));
        card.setSurveyPoint(pJson.getString("SurveyPoint"));
        card.setCarrier(pJson.getString("Carrier"));
        card.setDriver(pJson.getString("Driver"));

        return card;
    }
}
