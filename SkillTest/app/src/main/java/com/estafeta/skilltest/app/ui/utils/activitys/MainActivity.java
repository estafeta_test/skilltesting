package com.estafeta.skilltest.app.ui.utils.activitys;

import android.app.Fragment;
import android.app.FragmentTransaction;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.ViewGroup;

import com.estafeta.skilltest.app.R;
import com.estafeta.skilltest.app.ui.cardlist.CarCardListFragment;
import com.estafeta.skilltest.app.utils.log;

public class MainActivity extends RealmActivity {

    private static final String FRAGMENT_CLAZZ = "FragmentClazz";
    private static final String FRAGMENT_CLAZZ_ARGS = "FragmentClazzArgs";
    private static final String TAG = MainActivity.class.getSimpleName();

    private final String mMainClass = CarCardListFragment.class.getName();

    private Toolbar mToolBar;
    private ViewGroup mViewCrouton;
    private String mCurrentFragment;
    private Bundle mStoredArgs;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mToolBar = (Toolbar) findViewById(R.id.toolbar);
        mViewCrouton = (ViewGroup) findViewById(R.id.crouton);
        setSupportActionBar(mToolBar);

        if (savedInstanceState != null && savedInstanceState.containsKey(FRAGMENT_CLAZZ)) {
            Bundle args = null;
            if (savedInstanceState.containsKey(FRAGMENT_CLAZZ_ARGS)) {
                args = savedInstanceState.getBundle(FRAGMENT_CLAZZ_ARGS);
            };

            log.dt(TAG, String.format("args", args == null ? "null" : args.toString()));

            showPresentations(savedInstanceState.getString(FRAGMENT_CLAZZ), false, args);
        } else {
            showPresentations(mMainClass, false);
        }

    }

    @Override
    public void onBackPressed() {
        if (mMainClass.equals(mCurrentFragment)) {
            super.onBackPressed();
        } else {
            showPresentations(mMainClass, false);
        }
    }


    public void showPresentations(String classname, boolean pPopBackStack) {
        showPresentations(classname, pPopBackStack, null);
    }


    public void showPresentations(String classname, boolean pPopBackStack, Bundle pArgs) {
        FragmentTransaction ft = getFragmentManager().beginTransaction();
        ft.replace(R.id.container, Fragment.instantiate(this, classname, pArgs));
        if (pPopBackStack) {
            ft.addToBackStack(classname);
        }
        ft.commit();

        mCurrentFragment = classname;
        mStoredArgs = pArgs;
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        outState.putString(FRAGMENT_CLAZZ, mCurrentFragment);
        if (mStoredArgs != null) {
            outState.putBundle(FRAGMENT_CLAZZ_ARGS, mStoredArgs);
        }
        super.onSaveInstanceState(outState);
    }
}

