package com.estafeta.skilltest.app.request;

import android.support.v4.content.ContextCompat;

import com.estafeta.skilltest.app.R;
import com.estafeta.skilltest.app.application.ESTApplication;

/**
 * Created by Doosikus on 03.08.2016.
 * @author doosik
 * Статусы ошибок
 */
public enum EErrorMessage {
    E0(0, R.string.error0, android.R.color.holo_red_dark),
    EL1(1, R.string.error1, android.R.color.holo_red_dark),
    E403(403, R.string.error_403, android.R.color.holo_red_dark),
    E404(404, R.string.error404, android.R.color.holo_red_dark);

    private final int mStatusCode;
    private final int mMessage;
    private final int mColor;

    EErrorMessage(int pStatusCode, int pMessage, int pColor) {
        mStatusCode = pStatusCode;
        mMessage = pMessage;
        mColor = pColor;
    }

    public int getStatusCode() {
        return mStatusCode;
    }

    public int getMessage() {
        return mMessage;
    }

    public String getMessageString() {
        return ESTApplication.getContext().getResources().getString(mMessage);
    }

    public int getColor() {
        return ContextCompat.getColor(ESTApplication.getContext(), mColor);
    }

    public static EErrorMessage getByStatusCode(int pStatusCode) {
        for (EErrorMessage eErrorMessage : values()) {
            if (eErrorMessage.getStatusCode() == pStatusCode) {
                return eErrorMessage;
            }
        }
        return E0;
    }
}
