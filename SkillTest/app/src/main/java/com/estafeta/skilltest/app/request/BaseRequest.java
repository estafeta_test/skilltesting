package com.estafeta.skilltest.app.request;

import android.text.TextUtils;
import android.util.Base64;
import android.util.Log;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.HttpHeaderParser;
import com.estafeta.skilltest.app.BuildConfig;
import com.estafeta.skilltest.app.application.ESTApplication;
import com.estafeta.skilltest.app.utils.Settings;
import com.estafeta.skilltest.app.utils.log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.Map;

import io.realm.Realm;

/**
 * BaseRequest - базовый класс отправки реквестов
 */
public abstract class BaseRequest extends Request<JSONArray> {

    private static final String TAG = "Request";



    protected static final String PROTOCOL_CHARSET = "utf-8";
    private final boolean mAuth;

    public BaseRequest(int pMethod, String pUrl, boolean pAuth) {
        super(pMethod, Settings.HOST_SERVER + pUrl, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                int statusCode = (error == null || error.networkResponse == null) ? 0 : error.networkResponse.statusCode;
                EErrorMessage type = EErrorMessage.getByStatusCode(statusCode);
                postError(type, error.getLocalizedMessage());
            }
        });

        setRetryPolicy(new DefaultRetryPolicy(30 * 1000, getRetry(), 1.5f));
        mAuth = pAuth;

    }

    protected static void postError(EErrorMessage pType, Exception pException) {
        if (pException != null && !TextUtils.isEmpty(pException.getLocalizedMessage())) {
            postError(pType, pException.getLocalizedMessage());
        }
    }

    @Override
    public Map<String, String> getHeaders() throws AuthFailureError {
        if (mAuth) {
            Map<String, String> params = new HashMap<String, String>();
            params.put(
                    "Authorization",
                    String.format("Basic %s", Base64.encodeToString(
                            String.format("%s:%s", Settings.HOST_USER, Settings.HOST_PASS).getBytes(), Base64.DEFAULT)));
            log.dt(getTAGFormatted(), params.toString());
            return params;
        }
        return super.getHeaders();
    }

    public static void postError(EErrorMessage pType, String pException) {
        ///TODO бродкастить ошибки на уай в случае необходимости
        log.et(pType.getMessageString(), pException != null ? pException : "");

    }

    protected abstract int getRetry();
    protected JSONObject getJSONBody() {
        return null;
    };

    @Override
    public byte[] getBody() throws AuthFailureError {
        if (getJSONBody() == null) {
            return super.getBody();
        } else {
            try {
                return getJSONBody().toString().getBytes(PROTOCOL_CHARSET);
            } catch (UnsupportedEncodingException uee) {
                postError(EErrorMessage.EL1, uee);
                VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s",
                        getJSONBody().toString(), PROTOCOL_CHARSET);
                return super.getBody();
            }
        }
    }

    @Override
    protected final Response<JSONArray> parseNetworkResponse(NetworkResponse response) {
        String jsonString;
        try {

            //TODO залогировать
            jsonString = new String(response.data,
                    HttpHeaderParser.parseCharset(response.headers));
            log.dt(getTAGFormatted(), String.format("Status: %d, Data: %s", response.statusCode, jsonString));
            return Response.success(new JSONArray(jsonString),
                    HttpHeaderParser.parseCacheHeaders(response));
        } catch (Exception e) {
            return Response.error(new VolleyError(e.getMessage()));
        }

    }


    protected abstract void onResponse(Realm pRealm, JSONArray response) throws JSONException;

    @Override
    protected final void deliverResponse(JSONArray response) {
        if (response == null) {
            log.et(getTAGFormatted(), "responce is null");
            postError(EErrorMessage.E0, "responce is null");
        } else {
            try {
                Realm realm = Realm.getDefaultInstance();
                try {
                    onResponse(realm, response);
                } finally {
                    if (realm != null) {
                        realm.close();
                    }
                }

            } catch (JSONException e) {
                postError(EErrorMessage.E0, e);
                log.et(getTAGFormatted(), String.format("responce whit error %s", e.getLocalizedMessage()));

            }
        }
    }

    protected String getTAGFormatted() {
        return String.format("%s >>>> %s", TAG, getUrl());
    }
}
