package com.estafeta.skilltest.app.request;

import android.widget.Toast;

import com.android.volley.RequestQueue;
import com.estafeta.skilltest.app.application.ESTApplication;
import com.estafeta.skilltest.app.entity.helpers.CarCardHelper;
import com.estafeta.skilltest.app.utils.log;

import org.json.JSONArray;
import org.json.JSONException;

import io.realm.Realm;

/**
 * Created by Doosikus on 04.08.2016.
 * CarListRequest
 */

public class CarListRequest extends BaseRequest {

    public static final String URL = "mobilesurveytasks/gettestsurveytasks";

    public static void Send() {
        RequestQueue rq = ((ESTApplication)ESTApplication.getContext()).getRequestQueue();
        rq.cancelAll(URL);
        rq.add(new CarListRequest());
    }

    public CarListRequest() {
        super(Method.GET, URL, true);
    }

    //количество повторов
    @Override
    protected int getRetry() {
        return 0;
    }

    @Override
    protected void onResponse(Realm realm, JSONArray response) throws JSONException {
        log.dt(getTAGFormatted(), String.format("deliverResponse: %s", response.toString()));
        new CarCardHelper(realm).createOrUpdateAllFromJsonTransaction(response);
    }
}
