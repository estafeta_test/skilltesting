package com.estafeta.skilltest.app.entity.helpers;

import android.support.annotation.Nullable;

import com.estafeta.skilltest.app.utils.log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import io.realm.Realm;
import io.realm.RealmObject;
import io.realm.RealmQuery;
import io.realm.RealmResults;
import io.realm.exceptions.RealmException;

/**
 * Created by Doosikus on 03.08.2016.
 *
 * @author doosik
 *         Базовый класс на случай если сущностей больше чем одна, для удобства работы
 */
public abstract class BaseHelper<T extends RealmObject> {
    private static final String TAG = BaseHelper.class.getSimpleName();
    protected final Realm mRealm;
    protected final Class<T> mClazz;

    public BaseHelper(Realm pRealm, Class<T> pClazz) {
        mRealm = pRealm;
        mClazz = pClazz;
    }

    public Realm getRealm() {
        return mRealm;
    }

    protected void setInternalId(T pEntity) {
        //Override this is needed.
    }

    public void beginTransaction() {
        mRealm.beginTransaction();
    }

    public void commitTransaction() {
        mRealm.commitTransaction();
    }

    public void cancelTransaction() {
        mRealm.cancelTransaction();
    }

    /**
     * Возвращает сущность из базы по СЕРВЕРНОМУ ID
     *
     * @param entityId Серверный ID
     * @return сущность
     */
    @Nullable
    public T getEntityById(long entityId) {
        return query().equalTo("Id", entityId).findFirst();
    }

    /**
     * Возвращает сущность из базы по СЕРВЕРНОМУ ID
     *
     * @param pId Серверный ID
     * @return сущность
     */
    @Nullable
    public T getEntityById(String pId) {
        return query().equalTo("Id", pId).findFirst();
    }

    public long count() {
        return query().count();
    }

    /**
     * Create entity and set internal identificator
     */
    public T create() {
        T entity = mRealm.createObject(mClazz);
        setInternalId(entity);
        return entity;
    }

    public void delete(T pEntity) {
        pEntity.removeFromRealm();
    }

    public void delete(List<T> pEntitys) {
        final Object[] pEntityArray = pEntitys.toArray();
        delete(pEntityArray);
    }

    public void delete(Object[] pEntityArray) {
        for (Object anAll : pEntityArray) {
            T item = (T) anAll;
            item.removeFromRealm();
        }
    }

    public void deleteAll() {
        mRealm.clear(mClazz);
    }

    public RealmResults<T> findAll() {
        return mRealm.allObjects(mClazz);
    }

    public RealmQuery<T> query() {
        return mRealm.where(mClazz);
    }

    public T createOrUpdateFromJson(JSONObject pJson) throws RealmException, JSONException, ParseException {
        return mRealm.createOrUpdateObjectFromJson(mClazz, pJson);
    }

    public List<T> createOrUpdateAllFromJson(JSONArray pJson) throws JSONException, RealmException, ParseException {
        final int length = pJson.length();
        List<T> result = new ArrayList<>(length);

        for (int i = 0; i < length; i++) {
            T entity = createOrUpdateFromJson(pJson.getJSONObject(i));
            if (entity != null) {
                result.add(entity);
            }
        }

        return result;
    }

    public void createOrUpdateAllFromJsonTransaction(JSONArray pJson) throws JSONException, RealmException {
        try {
            beginTransaction();
            createOrUpdateAllFromJson(pJson);
            commitTransaction();
        } catch (RealmException pE) {
            cancelTransaction();
            log.et(TAG, pE.getLocalizedMessage());
        } catch (ParseException pE) {
            cancelTransaction();
            log.et(TAG, pE.getLocalizedMessage());
        }

    }


}
