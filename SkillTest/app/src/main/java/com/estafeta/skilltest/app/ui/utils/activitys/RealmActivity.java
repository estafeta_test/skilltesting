package com.estafeta.skilltest.app.ui.utils.activitys;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.estafeta.skilltest.app.application.ESTApplication;

import io.realm.Realm;

/**
 * Created by Doosikus on 04.08.2016.
 * RealmActivity
 */
public class RealmActivity extends AppCompatActivity {

    private Realm mRealm;
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mRealm = Realm.getDefaultInstance();
    }
    @Override
    protected void onDestroy() {
        if (mRealm != null) {
            mRealm.close();
            mRealm = null;
        }
        super.onDestroy();
    }

    public ESTApplication getApp() {
        return (ESTApplication) this.getApplication();
    }

    public Realm getRealm() {
        return mRealm;
    }

}
