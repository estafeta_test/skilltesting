package com.estafeta.skilltest.app.entity;

import java.util.Date;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by Doosikus on 03.08.2016.
 * CarCard - сущность карточки движения авто
 */
public class CarCard extends RealmObject{
    @PrimaryKey
    private long Id;
    private String Number;
    private Date PlannedStartDate;
    private Date PlannedEndDate;
    private Date ActualStartDate;
    private Date ActualEndDate;
    private String Vin;
    private String Model;
    private String ModelCode;
    private String Brand;
    private String SurveyPoint;
    private String Carrier;
    private String Driver;

    public Date getActualEndDate() {
        return ActualEndDate;
    }

    public void setActualEndDate(Date pActualEndDate) {
        ActualEndDate = pActualEndDate;
    }

    public Date getActualStartDate() {
        return ActualStartDate;
    }

    public void setActualStartDate(Date pActualStartDate) {
        ActualStartDate = pActualStartDate;
    }

    public String getCarrier() {
        return Carrier;
    }

    public void setCarrier(String pCarrier) {
        Carrier = pCarrier;
    }

    public String getDriver() {
        return Driver;
    }

    public void setDriver(String pDriver) {
        Driver = pDriver;
    }

    public long getId() {
        return Id;
    }

    public void setId(long pId) {
        Id = pId;
    }

    public String getModel() {
        return Model;
    }

    public void setModel(String pModel) {
        Model = pModel;
    }

    public String getModelCode() {
        return ModelCode;
    }

    public void setModelCode(String pModelCode) {
        ModelCode = pModelCode;
    }

    public String getNumber() {
        return Number;
    }

    public void setNumber(String pNumber) {
        Number = pNumber;
    }

    public Date getPlannedEndDate() {
        return PlannedEndDate;
    }

    public void setPlannedEndDate(Date pPlannedEndDate) {
        PlannedEndDate = pPlannedEndDate;
    }

    public Date getPlannedStartDate() {
        return PlannedStartDate;
    }

    public void setPlannedStartDate(Date pPlannedStartDate) {
        PlannedStartDate = pPlannedStartDate;
    }

    public String getBrand() {
        return Brand;
    }

    public void setBrand(String pBrand) {
        Brand = pBrand;
    }

    public String getSurveyPoint() {
        return SurveyPoint;
    }

    public void setSurveyPoint(String pSurveyPoint) {
        SurveyPoint = pSurveyPoint;
    }

    public String getVin() {
        return Vin;
    }

    public void setVin(String pVin) {
        Vin = pVin;
    }
}
